'use strict';

module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);

    grunt.loadNpmTasks('grunt-processhtml');
    grunt.loadNpmTasks('grunt-rsync');


	grunt.initConfig({

		sass: {
			dist: {
				options: {
					includePaths: ['<%= root %>/<%= app %>/bower_components/foundation/scss','<%= root %>/<%= app %>/bower_components/bourbon/app/assets/stylesheets'],
					quiet: true
				},
				files: {
					'<%= root %>/<%= app %>/css/app.css': '<%= root %>/<%= app %>/scss/app.scss'
				}
			}
		},

		rsync: {
		    options: {
		        args: ['-av'],
		        exclude: ['.git*','*.scss','node_modules'],
		        recursive: true
		    }
		},
		

		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			all: [
				'Gruntfile.js',
				'<%= root %>/<%= app %>/js/**/*.js'
			]
		},

		clean: {
			dist: {
				src: ['<%= dist %>/*']
			},
		},
		copy: {
			dist: {
				files: [{
					expand: true,
					cwd:'<%= root %>/',
					src: ['**/*', '!**/bower_components/**', '!**/scss/**'],
					dest: '<%= dist %>/'
				}]
			},
		},

		uglify: {
			options: {
				preserveComments: 'some',
				mangle: false
			}
		},

		useminPrepare: {
			html: ['<%= root %>/<%= app %>/index.html'],
			options: {
				dest: '<%= dist %>/<%= app %>'
			}
		},

		usemin: {
			html: ['<%= dist %>/**/*.html', '!<%= root %>/<%= app %>/bower_components/**'],
			css: ['<%= dist %>/<%= app %>/css/**/*.css'],
			options: {
				dirs: ['<%= dist %>/<%= app %>']
			}
		},

		watch: {
			grunt: {
				files: ['Gruntfile.js'],
				tasks: ['sass']
			},
			sass: {
				files: '<%= root %>/<%= app %>/scss/**/*.scss',
				tasks: ['sass']
			},
			livereload: {
				files: ['<%= root %>/<%= app %>/**/*.html', '!<%= root %>/<%= app %>/bower_components/**', '<%= root %>/<%= app %>/js/**/*.js', '<%= root %>/<%= app %>/css/**/*.css', '<%= root %>/<%= app %>/images/**/*.{jpg,gif,svg,jpeg,png}'],
				options: {
					livereload: true
				}
			}
		},

		connect: {
			app: {
				options: {
					port: 9000,
					base: '<%= root %>/<%= app %>/',
					open: true,
					livereload: true,
					hostname: '127.0.0.1'
				}
			},
			dist: {
				options: {
					port: 9001,
					base: '<%= dist %>/',
					open: true,
					keepalive: true,
					livereload: false,
					hostname: '127.0.0.1'
				}
			}
		},

		wiredep: {
			target: {
				src: [
					'<%= root %>/<%= app %>/**/*.html'
				],
				exclude: [
					'modernizr',
					'jquery-placeholder',
					'jquery.cookie',
					'foundation'
				]
			}
		},

		processhtml: {
		    options: {
		      customBlockTypes: ['custom-blocktype.js']
		    },
		    dist: {
		      files: {
		        '<%= dist %>/<%= app %>/index.html': ['<%= dist %>/<%= app %>/index.html']
		      }
		    },
		}

	});

	
	grunt.registerTask('compile-sass', ['sass']);
	grunt.registerTask('bower-install', ['wiredep']);
	
	grunt.registerTask('default', ['compile-sass', 'bower-install', 'connect:app', 'watch']);
	grunt.registerTask('validate-js', ['jshint']);
	grunt.registerTask('server-dist', ['connect:dist']);
	
	// @todo: uglifier wieder aktivieren!
	grunt.registerTask('publish', ['compile-sass', 'clean:dist', 'useminPrepare', 'copy:dist', 'concat', 'cssmin', 'uglify', 'usemin', 'processhtml']);

};
