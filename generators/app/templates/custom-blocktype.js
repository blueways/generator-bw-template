'use strict';

module.exports = function (processor) {
  // This will allow to use this <!-- build:customBlock[:target] <value> --> syntax
  processor.registerBlockType('cObject', function (content, block, blockLine, blockContent) {
    var fluid = '<f:cObject typoscriptObjectPath="'+block.asset+'" />';
    var result = content.replace(blockLine, fluid);

    return result;
  });

  processor.registerBlockType('content', function (content, block, blockLine, blockContent) {

    var fluid = '<f:format.html parseFuncTSPath="">'+block.asset+'</f:format.html>';
    var result = content.replace(blockLine, fluid);
    
    return result;
  });
};