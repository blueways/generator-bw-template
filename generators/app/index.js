'use strict';
var yeoman = require('yeoman-generator');
var chalk = require('chalk');
var yosay = require('yosay');

module.exports = yeoman.generators.Base.extend({
  prompting: function () {
    var done = this.async();

    // Have Yeoman greet the user.
    this.log(yosay(
      'Welcome to the grand ' + chalk.red('BwTemplate') + ' generator!'
    ));

    var prompts = [{
      type: 'input',
      name: 'appname',
      message: 'Folder name for main template inside template dir',
      default: 'app'
    },
    {
      type: 'confirm',
      name: 'typoscript',
      message: 'Would you like to use Typoscript in your project?',
      default: true
    },
    {
      type: 'confirm',
      name: 'typoscriptDefaultConf',
      message: 'Would you like to include default typoscript configuration?',
      when: function(input){
        return input.typoscript;
      }
    },
    {
      type: 'confirm',
      name: 'lotte',
      message: 'Would you like to configure ssh rsync to lotte? (`lotte` is used as ssh host name)',
      default: false
    },
    {
      type: 'input',
      name: 'lotteDest',
      message: 'Destination dir on lotte? Ex. '+chalk.yellow('/home/blueways/vhosts/')+''+chalk.red('geo.hs-anhalt.lotte.blueways.de')+''+chalk.yellow('/htdocs/fileadmin/templates/'),
      when: function(input){
        return input.lotte;
      }
    },
    {
      type: 'confirm',
      name: 'live',
      message: 'Would you like to configure ssh rsync to live server?',
      default: false
    },
    {
      type: 'input',
      name: 'liveHost',
      message: 'What is the ssh host name in your ssh.conf?',
      when: function(input){
        return input.live;
      }
    },
    {
      type: 'input',
      name: 'liveDest',
      message: 'Destination dir on live server? Ex. '+chalk.yellow('/var/www/htdocs/fileadmin/templates/'),
      when: function(input){
        return input.live;
      }
    }];

    this.prompt(prompts, function (props) {
      this.props = props;
      // To access props later use this.props.someOption;

      done();
    }.bind(this));
  },

  writing: {
    app: function () {
      this.fs.copy(
        this.templatePath('_package.json'),
        this.destinationPath('package.json')
      );
      this.fs.copyTpl(
        this.templatePath('bowerrc'),
        this.destinationPath('.bowerrc'),
        { app: this.props.appname }
      );
      this.fs.copyTpl(
        this.templatePath('gitignore'),
        this.destinationPath('.gitignore'),
        { app: this.props.appname }
      );
      this.fs.copy(
        this.templatePath('_bower.json'),
        this.destinationPath('bower.json')
      );
      this.fs.copy(
        this.templatePath('Gruntfile.js'),
        this.destinationPath('Gruntfile.js')
      );
      this.fs.copy(
        this.templatePath('custom-blocktype.js'),
        this.destinationPath('custom-blocktype.js')
      );

      this.gruntfile.insertConfig('root', '"templates"');
      this.gruntfile.insertConfig('app', '"'+this.props.appname+'"');
      this.gruntfile.insertConfig('dist', '"dist"');

      // set rsync config and hosts
      var rsyncConf = "options: { args: ['-av'], exclude: ['.git*','*.scss','node_modules'], recursive: true }";
      if(this.props.lotte) rsyncConf += ", lotte: { options: { src: '<%= dist %>/', dest: '"+this.props.lotteDest+"', host: 'lotte'} }";
      if(this.props.live) rsyncConf += ", live: { options: { src: '<%= dist %>/', dest: '"+this.props.liveDest+"', host: '"+this.props.liveHost+"'} }";
      this.gruntfile.insertConfig('rsync', "{"+rsyncConf+"}");
    },

    projectfiles: function () {
      //var appdir = this.destinationRoot() + '/templates/' + this.props.appname + '/';
      this.fs.copy(
        this.templatePath('appstyles.scss'),
        this.destinationPath('templates/' + this.props.appname + '/scss/_appstyles.scss')
      );
      this.fs.copy(
        this.templatePath('app.js'),
        this.destinationPath('templates/' + this.props.appname + '/js/app.js')
      );
      this.fs.copy(
        this.templatePath('index.html'),
        this.destinationPath('templates/' + this.props.appname + '/index.html')
      );
    }
  },

  install: function () {
    this.installDependencies();
  },

  end: function () {
    var appdir = this.destinationRoot() + '/templates/' + this.props.appname + '/';
    // copy settings.scss
    this.fs.copy(
      appdir + 'bower_components/foundation/scss/foundation/_settings.scss',
      this.destinationPath('templates/' + this.props.appname + '/scss/_settings.scss')
    );
    // create app.scss
    var appScss = '@import "bourbon";\n@import "normalize";\n@import "settings";\n@charset "UTF-8";\n';
    appScss += this.fs.read(appdir + 'bower_components/foundation/scss/foundation.scss');
    appScss += '\n@import "appstyles";';
    this.fs.write(this.destinationPath('templates/' + this.props.appname + '/scss/app.scss'), appScss);
  }
});
